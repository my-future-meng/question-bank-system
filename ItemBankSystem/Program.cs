﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Forms.LoginForms;


/*
 * 来自于--xiaohanxixi
 * 码云Gitee:https://gitee.com/xiaohanxixi
 */

namespace ItemBankSystem
{
	static class Program
	{
		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new LoginFrom());
		}
	}
}
