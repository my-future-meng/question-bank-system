﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItemBankSystem.Classes
{
	class TestPaperClass
	{
		private int paperid;
		private string classes = null;
		private string papertitle = null;

		public int Paperid { get => paperid; set => paperid = value; }
		public string Classes { get => classes; set => classes = value; }
		public string Papertitle { get => papertitle; set => papertitle = value; }

		public TestPaperClass()
		{

		}
		/// <summary>
		/// 试卷
		/// </summary>
		/// <param name="id">试卷编号</param>
		/// <param name="classes">课程</param>
		/// <param name="title">试卷标题</param>
		public TestPaperClass(int id, string classes, string title)
		{
			paperid = id;
			this.classes = classes;
			papertitle = title;
		}

		/// <summary>
		/// 删除试题
		/// </summary>
		/// <returns>是否删除成功</returns>
		public bool Paper_Delete()
		{
			try
			{
				SqlServerEmploy sqlServer = new SqlServerEmploy();
				if (sqlServer.Getsqlcom("delete tb_Test_Paper where paper_id=" + Paperid) > 0 && sqlServer.Getsqlcom("delete tb_Test_Paper_Single where paper_id=" + Paperid) > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch
			{
				return false;
			}

		}
	}
}
