﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItemBankSystem.Classes;



namespace ItemBankSystem.Classes
{
	public class UserClass
	{
		private string user = null;
		private int type;
		public string User { get => user; set => user = value; }
		public int Type { get => type; set => type = value; }
		public UserClass()
		{

		}
		public UserClass(string user, int type)
		{
			User = user;
			Type = type;
		}
		/// <summary>
		/// 修改账户密码
		/// </summary>
		/// <param name="passward">密码</param>
		/// <returns>返回是否成功</returns>
		public bool update_Passward(string passward)
		{
			SqlServerEmploy sqlServer = new SqlServerEmploy();
			if (sqlServer.Getsqlcom("Update tb_Users set User_passward='" + passward + "' where User_id='" + User + "'") > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除用户
		/// </summary>
		/// <returns></returns>
		public bool delete_user()
		{
			SqlServerEmploy sqlServer = new SqlServerEmploy();
			if (sqlServer.Getsqlcom("delete tb_Users where User_id='" + User + "'") > 0)
			{
				User = string.Empty;
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 修改权限
		/// </summary>
		/// <param name="type">修改成什么权限</param>
		/// <returns></returns>
		public bool update_type(int type)
		{
			SqlServerEmploy sqlServer = new SqlServerEmploy();
			if (sqlServer.Getsqlcom("Update tb_Users set User_type=" + type + " where User_id='" + User + "'") > 0)
			{
				User = string.Empty;
				return true;
			}
			else
			{
				return false;
			}
		}

	}
}
