﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ItemBankSystem.Classes
{
	class TestQuestionClass
	{
		private int questionid;
		private string classes = null;
		private string questiontest = null;
		private string questionanswer = null;
		private string questiontype = null;
		private int questiontime;

		public int Questionid { get => questionid; set => questionid = value; }
		public string Classes { get => classes; set => classes = value; }
		public string Questiontest { get => questiontest; set => questiontest = value; }
		public string Questionanswer { get => questionanswer; set => questionanswer = value; }
		public string Questiontype { get => questiontype; set => questiontype = value; }
		public int Questiontime { get => questiontime; set => questiontime = value; }

		public TestQuestionClass()
		{

		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="id">试题编号</param>
		/// <param name="classes">课程</param>
		/// <param name="test">试题内容</param>
		/// <param name="answer">试题答案</param>
		/// <param name="type">试题类型</param>
		/// <param name="time">添加时间</param>
		public TestQuestionClass(int id, string classes, string test, string answer, string type, int time)
		{
			questionid = id;
			this.classes = classes;
			questiontest = test;
			questionanswer = answer;
			questiontype = type;
			questiontime = time;
		}
		/// <summary>
		/// 删除试题
		/// </summary>
		/// <returns>是否删除成功</returns>
		public bool Test_Delete()
		{
			try
			{
				SqlServerEmploy sqlServer = new SqlServerEmploy();
				if (sqlServer.Getsqlcom("delete tb_Question_Library where question_id=" + Questionid) > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch 
			{
				return false;
			}

		}

	}
}
