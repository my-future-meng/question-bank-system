﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ItemBankSystem.Classes
{
	class SqlServerEmploy
	{
		private SqlConnection sqlConnection;
		private SqlCommand sqlCom;

		public SqlConnection SqlConnect { get => sqlConnection; set => sqlConnection = value; }
		public SqlCommand SqlCom { get => sqlCom; set => sqlCom = value; }

		public SqlServerEmploy()
		{
			string strCon = "Data Source=" + PublicStaticClass.Sql_server_ip + ";Initial Catalog=ItemBook;User ID=" + PublicStaticClass.Sql_server_name + ";Password=" + PublicStaticClass.Sql_server_passward;
			try
			{
				SqlConnect = new SqlConnection(strCon);
				SqlCom = new SqlCommand();
				SqlCom.Connection = SqlConnect;
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		/// <summary>
		/// 修改数据库
		/// </summary>
		/// <param name="SQLstr">修改字符串</param>
		/// <returns>受影响的行数</returns>
		public int Getsqlcom(string SQLstr)
		{
			int row;
			if (SqlConnect.State == ConnectionState.Closed)
			{
				SqlConnect.Open();
			}
			SqlCommand SQLcom = new SqlCommand(SQLstr, SqlConnect);
			row = SQLcom.ExecuteNonQuery();
			SQLcom.Dispose();
			return row;
		}
		/// <summary>
		/// 读取或更新数据表的内容
		/// </summary>
		/// <param name="SQLstr">字符串</param>
		/// <returns>返回DataSet表</returns>
		public DataSet GetDataSet(string SQLstr)
		{
			if (SqlConnect.State == ConnectionState.Closed)
			{
				SqlConnect.Open();
			}
			SqlDataAdapter sqlda = new SqlDataAdapter(SQLstr, SqlConnect);
			DataSet dataSet = new DataSet();
			sqlda.Fill(dataSet);
			return dataSet;
		}
		/// <summary>
		/// 查询数据库
		/// </summary>
		/// <param name="strSql">查询字符串</param>
		/// <returns>返回</returns>
		public SqlDataReader GetSqlDataReader(string strSql)
		{
			SqlDataReader sdr;
			SqlCom.CommandType = CommandType.Text;
			SqlCom.CommandText = strSql;

			if (SqlConnect.State == ConnectionState.Closed)
			{
				SqlConnect.Open();
			}
			sdr = SqlCom.ExecuteReader(CommandBehavior.CloseConnection);
			//sdr对象和m_Conn对象暂时不能关闭和释放掉，否则在调用时无法使用
			//待使用完毕sdr，再关闭sdr对象（同时会自动关闭关联的m_Conn对象）
			//m_Conn的关闭是指关闭连接通道，但连接对象依然存在
			//m_Conn的释放掉是指销毁连接对象
			return sdr;
		}
	}
}
