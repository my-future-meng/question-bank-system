﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItemBankSystem.Forms.TestQuestionForms;

namespace ItemBankSystem.Classes
{
	class PublicStaticClass
	{
		/// <summary>
		/// 查询试题窗体
		/// </summary>
		public static FindTestQuestionForm FindTestQuestion;
		/// <summary>
		/// 浏览试题窗体
		/// </summary>
		public static LookTestForm LookTest;
		/// <summary>
		/// 添加试题窗体
		/// </summary>
		public static AddTestQuestionForm AddTestQuestion;

		/// <summary>
		/// 列表选择的试题
		/// </summary>
		public static TestQuestionClass TestQuestion;
		/// <summary>
		/// 列表选择的试卷
		/// </summary>
		public static TestPaperClass TestPaper;
		/// <summary>
		/// 选择的窗体名
		/// </summary>
		public static string Form_name = null;

		public static string datetime = null;
		/// <summary>
		/// 选择的按钮
		/// </summary>
		public static string change_btn = null;
		
		public static string Sql_server_ip = null;
		public static string Sql_server_name = null;
		public static string Sql_server_passward = null;

		public static UserClass user;

		/// <summary>
		/// 字符数组合并并插入特定字符
		/// </summary>
		/// <param name="strs">字符数组</param>
		/// <param name="th">特定字符</param>
		/// <returns>字符串</returns>
		public static string TransForms(List<string> strs, string th)
		{
			string str = null;
			bool a = true;
			for (int i = 0; i < strs.Count; i++)
			{
				if (strs[i] != "")
				{
					if (a)
					{
						str = strs[i];
						a = false;
					}
					else
					{
						str += th + strs[i];
					}
				}
			}
			return str;
		}
	}
}
