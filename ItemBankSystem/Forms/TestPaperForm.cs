﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Forms.TestPaperForms;

namespace ItemBankSystem.Forms
{
	public partial class TestPaperForm : Form
	{
		public TestPaperForm()
		{
			InitializeComponent();
		}
		//试卷生成
		private void btn_SJcreate_Click(object sender, EventArgs e)
		{
			splitContainer1.Panel2.Controls.Clear();
		 	AddTestPaperForm addTestPaperForm = new AddTestPaperForm()
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(addTestPaperForm);
			addTestPaperForm.Show();
		}
		//浏览试卷
		private void tbn_LL_Click(object sender, EventArgs e)
		{
			splitContainer1.Panel2.Controls.Clear();
			LookTestPaperForm lookTestPaperForm = new LookTestPaperForm()
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(lookTestPaperForm);
			lookTestPaperForm.Show();
		}
	}
}
