﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Forms.TestQuestionForms;
using ItemBankSystem.Classes;

namespace ItemBankSystem.Forms
{
	public partial class TestQuestionForm : Form
	{
		public TestQuestionForm()
		{
			InitializeComponent();
		}
		//浏览试题
		private void btn_LL_Click(object sender, EventArgs e)
		{
			splitContainer1.Panel2.Controls.Clear();
			PublicStaticClass.LookTest = new LookTestForm
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(PublicStaticClass.LookTest);
			PublicStaticClass.LookTest.Show();
		}
		//查询试题
		private void btn_CX_Click(object sender, EventArgs e)
		{
			splitContainer1.Panel2.Controls.Clear();
			PublicStaticClass.FindTestQuestion = new FindTestQuestionForm
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(PublicStaticClass.FindTestQuestion);
			PublicStaticClass.FindTestQuestion.Show();
		}
		//添加试题
		private void tbn_TJ_Click(object sender, EventArgs e)
		{
			splitContainer1.Panel2.Controls.Clear();
			PublicStaticClass.AddTestQuestion = new AddTestQuestionForm
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(PublicStaticClass.AddTestQuestion);
			PublicStaticClass.AddTestQuestion.Show();
		}

		private void TestQuestionForm_Load(object sender, EventArgs e)
		{
			if (PublicStaticClass.user.Type == 1)
			{
				tbn_TJ.Enabled = false;
			}
		}
	}
}
