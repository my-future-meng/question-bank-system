﻿namespace ItemBankSystem.Forms
{
	partial class TestPaperForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.btn_SJcreate = new System.Windows.Forms.Button();
			this.tbn_LL = new System.Windows.Forms.Button();
			this.lbl_paperid = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.Silver;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
			this.splitContainer1.Size = new System.Drawing.Size(834, 461);
			this.splitContainer1.SplitterDistance = 156;
			this.splitContainer1.TabIndex = 2;
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
			this.flowLayoutPanel1.Controls.Add(this.tbn_LL);
			this.flowLayoutPanel1.Controls.Add(this.btn_SJcreate);
			this.flowLayoutPanel1.Controls.Add(this.lbl_paperid);
			this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(156, 461);
			this.flowLayoutPanel1.TabIndex = 0;
			// 
			// btn_SJcreate
			// 
			this.btn_SJcreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_SJcreate.Font = new System.Drawing.Font("新宋体", 10F);
			this.btn_SJcreate.Location = new System.Drawing.Point(0, 49);
			this.btn_SJcreate.Margin = new System.Windows.Forms.Padding(0);
			this.btn_SJcreate.Name = "btn_SJcreate";
			this.btn_SJcreate.Size = new System.Drawing.Size(156, 49);
			this.btn_SJcreate.TabIndex = 0;
			this.btn_SJcreate.Text = "试卷生成";
			this.btn_SJcreate.UseVisualStyleBackColor = true;
			this.btn_SJcreate.Click += new System.EventHandler(this.btn_SJcreate_Click);
			// 
			// tbn_LL
			// 
			this.tbn_LL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tbn_LL.Font = new System.Drawing.Font("新宋体", 10F);
			this.tbn_LL.Location = new System.Drawing.Point(0, 0);
			this.tbn_LL.Margin = new System.Windows.Forms.Padding(0);
			this.tbn_LL.Name = "tbn_LL";
			this.tbn_LL.Size = new System.Drawing.Size(156, 49);
			this.tbn_LL.TabIndex = 2;
			this.tbn_LL.Text = "浏览试卷";
			this.tbn_LL.UseVisualStyleBackColor = true;
			this.tbn_LL.Click += new System.EventHandler(this.tbn_LL_Click);
			// 
			// lbl_paperid
			// 
			this.lbl_paperid.AutoSize = true;
			this.lbl_paperid.Location = new System.Drawing.Point(3, 98);
			this.lbl_paperid.Name = "lbl_paperid";
			this.lbl_paperid.Size = new System.Drawing.Size(0, 12);
			this.lbl_paperid.TabIndex = 5;
			// 
			// TestPaperForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(834, 461);
			this.ControlBox = false;
			this.Controls.Add(this.splitContainer1);
			this.Name = "TestPaperForm";
			this.Text = "TestPaperForm";
			this.splitContainer1.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Button btn_SJcreate;
		private System.Windows.Forms.Button tbn_LL;
		private System.Windows.Forms.Label lbl_paperid;
	}
}