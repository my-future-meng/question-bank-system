﻿namespace ItemBankSystem.Forms
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.试题库管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.试卷管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.试题管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.系统维护ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.用户设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.管理员设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.退出系统ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.tssl_user = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
			this.tssl_type = new System.Windows.Forms.ToolStripStatusLabel();
			this.menuStrip1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.试题库管理ToolStripMenuItem,
            this.试卷管理ToolStripMenuItem,
            this.试题管理ToolStripMenuItem,
            this.系统维护ToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1235, 35);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// 试题库管理ToolStripMenuItem
			// 
			this.试题库管理ToolStripMenuItem.Name = "试题库管理ToolStripMenuItem";
			this.试题库管理ToolStripMenuItem.Padding = new System.Windows.Forms.Padding(15, 5, 15, 5);
			this.试题库管理ToolStripMenuItem.Size = new System.Drawing.Size(102, 31);
			this.试题库管理ToolStripMenuItem.Text = "试题库管理";
			this.试题库管理ToolStripMenuItem.Click += new System.EventHandler(this.试题库管理ToolStripMenuItem_Click);
			// 
			// 试卷管理ToolStripMenuItem
			// 
			this.试卷管理ToolStripMenuItem.Name = "试卷管理ToolStripMenuItem";
			this.试卷管理ToolStripMenuItem.Padding = new System.Windows.Forms.Padding(15, 5, 15, 5);
			this.试卷管理ToolStripMenuItem.Size = new System.Drawing.Size(90, 31);
			this.试卷管理ToolStripMenuItem.Text = "试卷管理";
			this.试卷管理ToolStripMenuItem.Click += new System.EventHandler(this.试卷管理ToolStripMenuItem_Click);
			// 
			// 试题管理ToolStripMenuItem
			// 
			this.试题管理ToolStripMenuItem.Name = "试题管理ToolStripMenuItem";
			this.试题管理ToolStripMenuItem.Padding = new System.Windows.Forms.Padding(15, 5, 15, 5);
			this.试题管理ToolStripMenuItem.Size = new System.Drawing.Size(90, 31);
			this.试题管理ToolStripMenuItem.Text = "试题管理";
			this.试题管理ToolStripMenuItem.Click += new System.EventHandler(this.试题管理ToolStripMenuItem_Click);
			// 
			// 系统维护ToolStripMenuItem
			// 
			this.系统维护ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.用户设置ToolStripMenuItem,
            this.管理员设置ToolStripMenuItem,
            this.退出系统ToolStripMenuItem});
			this.系统维护ToolStripMenuItem.Name = "系统维护ToolStripMenuItem";
			this.系统维护ToolStripMenuItem.Padding = new System.Windows.Forms.Padding(15, 5, 15, 5);
			this.系统维护ToolStripMenuItem.Size = new System.Drawing.Size(90, 31);
			this.系统维护ToolStripMenuItem.Text = "系统维护";
			// 
			// 用户设置ToolStripMenuItem
			// 
			this.用户设置ToolStripMenuItem.Name = "用户设置ToolStripMenuItem";
			this.用户设置ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.用户设置ToolStripMenuItem.Text = "用户设置";
			this.用户设置ToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
			this.用户设置ToolStripMenuItem.Click += new System.EventHandler(this.用户设置ToolStripMenuItem_Click);
			// 
			// 管理员设置ToolStripMenuItem
			// 
			this.管理员设置ToolStripMenuItem.Name = "管理员设置ToolStripMenuItem";
			this.管理员设置ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.管理员设置ToolStripMenuItem.Text = "管理员设置";
			this.管理员设置ToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
			this.管理员设置ToolStripMenuItem.Click += new System.EventHandler(this.管理员设置ToolStripMenuItem_Click);
			// 
			// 退出系统ToolStripMenuItem
			// 
			this.退出系统ToolStripMenuItem.Name = "退出系统ToolStripMenuItem";
			this.退出系统ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.退出系统ToolStripMenuItem.Text = "退出系统";
			this.退出系统ToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
			this.退出系统ToolStripMenuItem.Click += new System.EventHandler(this.退出系统ToolStripMenuItem_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tssl_user,
            this.toolStripStatusLabel2,
            this.tssl_type});
			this.statusStrip1.Location = new System.Drawing.Point(0, 680);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(1235, 22);
			this.statusStrip1.TabIndex = 2;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(56, 17);
			this.toolStripStatusLabel1.Text = "用户名：";
			// 
			// tssl_user
			// 
			this.tssl_user.Name = "tssl_user";
			this.tssl_user.Size = new System.Drawing.Size(0, 17);
			// 
			// toolStripStatusLabel2
			// 
			this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
			this.toolStripStatusLabel2.Size = new System.Drawing.Size(44, 17);
			this.toolStripStatusLabel2.Text = "权限：";
			// 
			// tssl_type
			// 
			this.tssl_type.Name = "tssl_type";
			this.tssl_type.Size = new System.Drawing.Size(0, 17);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1235, 702);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.menuStrip1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.IsMdiContainer = true;
			this.MainMenuStrip = this.menuStrip1;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "C语言试题库系统";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem 试卷管理ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 试题库管理ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 系统维护ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 用户设置ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 管理员设置ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 退出系统ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 试题管理ToolStripMenuItem;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
		private System.Windows.Forms.ToolStripStatusLabel tssl_user;
		private System.Windows.Forms.ToolStripStatusLabel tssl_type;
	}
}