﻿namespace ItemBankSystem.Forms.LoginForms
{
	partial class SigninForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbl_user = new System.Windows.Forms.Label();
			this.lbl_passward = new System.Windows.Forms.Label();
			this.Btn_Signin = new System.Windows.Forms.Button();
			this.Btn_return = new System.Windows.Forms.Button();
			this.txt_user = new System.Windows.Forms.TextBox();
			this.txt_passward = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// lbl_user
			// 
			this.lbl_user.AutoSize = true;
			this.lbl_user.Location = new System.Drawing.Point(19, 45);
			this.lbl_user.Name = "lbl_user";
			this.lbl_user.Size = new System.Drawing.Size(41, 12);
			this.lbl_user.TabIndex = 0;
			this.lbl_user.Text = "账号：";
			// 
			// lbl_passward
			// 
			this.lbl_passward.AutoSize = true;
			this.lbl_passward.Location = new System.Drawing.Point(19, 73);
			this.lbl_passward.Name = "lbl_passward";
			this.lbl_passward.Size = new System.Drawing.Size(41, 12);
			this.lbl_passward.TabIndex = 1;
			this.lbl_passward.Text = "密码：";
			// 
			// Btn_Signin
			// 
			this.Btn_Signin.Location = new System.Drawing.Point(66, 100);
			this.Btn_Signin.Name = "Btn_Signin";
			this.Btn_Signin.Size = new System.Drawing.Size(125, 24);
			this.Btn_Signin.TabIndex = 2;
			this.Btn_Signin.Text = "注册";
			this.Btn_Signin.UseVisualStyleBackColor = true;
			this.Btn_Signin.Click += new System.EventHandler(this.Btn_Signin_Click);
			// 
			// Btn_return
			// 
			this.Btn_return.Location = new System.Drawing.Point(66, 130);
			this.Btn_return.Name = "Btn_return";
			this.Btn_return.Size = new System.Drawing.Size(125, 24);
			this.Btn_return.TabIndex = 3;
			this.Btn_return.Text = "返回";
			this.Btn_return.UseVisualStyleBackColor = true;
			this.Btn_return.Click += new System.EventHandler(this.Btn_return_Click);
			// 
			// txt_user
			// 
			this.txt_user.Location = new System.Drawing.Point(66, 42);
			this.txt_user.MaxLength = 15;
			this.txt_user.Name = "txt_user";
			this.txt_user.Size = new System.Drawing.Size(125, 21);
			this.txt_user.TabIndex = 4;
			// 
			// txt_passward
			// 
			this.txt_passward.Location = new System.Drawing.Point(66, 70);
			this.txt_passward.MaxLength = 15;
			this.txt_passward.Name = "txt_passward";
			this.txt_passward.Size = new System.Drawing.Size(125, 21);
			this.txt_passward.TabIndex = 5;
			// 
			// SigninForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(247, 166);
			this.ControlBox = false;
			this.Controls.Add(this.txt_passward);
			this.Controls.Add(this.txt_user);
			this.Controls.Add(this.Btn_return);
			this.Controls.Add(this.Btn_Signin);
			this.Controls.Add(this.lbl_passward);
			this.Controls.Add(this.lbl_user);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "SigninForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "注册账号";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lbl_user;
		private System.Windows.Forms.Label lbl_passward;
		private System.Windows.Forms.Button Btn_Signin;
		private System.Windows.Forms.Button Btn_return;
		private System.Windows.Forms.TextBox txt_user;
		private System.Windows.Forms.TextBox txt_passward;
	}
}