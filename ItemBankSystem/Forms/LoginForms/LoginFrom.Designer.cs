﻿namespace ItemBankSystem.Forms.LoginForms
{
	partial class LoginFrom
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginFrom));
			this.Btn_login = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txt_user = new System.Windows.Forms.TextBox();
			this.txt_passward = new System.Windows.Forms.TextBox();
			this.Lkbl_Signin = new System.Windows.Forms.LinkLabel();
			this.label3 = new System.Windows.Forms.Label();
			this.Btn_out = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// Btn_login
			// 
			this.Btn_login.BackColor = System.Drawing.Color.DodgerBlue;
			this.Btn_login.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.Btn_login.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.Btn_login.Location = new System.Drawing.Point(126, 161);
			this.Btn_login.Name = "Btn_login";
			this.Btn_login.Size = new System.Drawing.Size(150, 35);
			this.Btn_login.TabIndex = 0;
			this.Btn_login.Text = "登录";
			this.Btn_login.UseVisualStyleBackColor = false;
			this.Btn_login.Click += new System.EventHandler(this.Btn_login_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(79, 109);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(41, 12);
			this.label1.TabIndex = 2;
			this.label1.Text = "账号：";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(79, 137);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 12);
			this.label2.TabIndex = 3;
			this.label2.Text = "密码：";
			// 
			// txt_user
			// 
			this.txt_user.Location = new System.Drawing.Point(126, 106);
			this.txt_user.MaxLength = 15;
			this.txt_user.Name = "txt_user";
			this.txt_user.Size = new System.Drawing.Size(150, 21);
			this.txt_user.TabIndex = 4;
			// 
			// txt_passward
			// 
			this.txt_passward.Location = new System.Drawing.Point(126, 134);
			this.txt_passward.MaxLength = 15;
			this.txt_passward.Name = "txt_passward";
			this.txt_passward.PasswordChar = '*';
			this.txt_passward.Size = new System.Drawing.Size(150, 21);
			this.txt_passward.TabIndex = 5;
			// 
			// Lkbl_Signin
			// 
			this.Lkbl_Signin.AutoSize = true;
			this.Lkbl_Signin.LinkColor = System.Drawing.Color.DimGray;
			this.Lkbl_Signin.Location = new System.Drawing.Point(283, 109);
			this.Lkbl_Signin.Name = "Lkbl_Signin";
			this.Lkbl_Signin.Size = new System.Drawing.Size(53, 12);
			this.Lkbl_Signin.TabIndex = 6;
			this.Lkbl_Signin.TabStop = true;
			this.Lkbl_Signin.Text = "注册账号";
			this.Lkbl_Signin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Lkbl_Signin_LinkClicked);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.label3.Font = new System.Drawing.Font("宋体", 25F);
			this.label3.Location = new System.Drawing.Point(66, 35);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(270, 34);
			this.label3.TabIndex = 7;
			this.label3.Text = "C语言试题库系统";
			// 
			// Btn_out
			// 
			this.Btn_out.BackColor = System.Drawing.Color.DodgerBlue;
			this.Btn_out.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.Btn_out.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.Btn_out.Location = new System.Drawing.Point(126, 202);
			this.Btn_out.Name = "Btn_out";
			this.Btn_out.Size = new System.Drawing.Size(150, 35);
			this.Btn_out.TabIndex = 8;
			this.Btn_out.Text = "退出";
			this.Btn_out.UseVisualStyleBackColor = false;
			this.Btn_out.Click += new System.EventHandler(this.Btn_out_Click);
			// 
			// LoginFrom
			// 
			this.AcceptButton = this.Btn_login;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(405, 239);
			this.ControlBox = false;
			this.Controls.Add(this.Btn_out);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.Lkbl_Signin);
			this.Controls.Add(this.txt_passward);
			this.Controls.Add(this.txt_user);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.Btn_login);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "LoginFrom";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "试题库系统";
			this.Load += new System.EventHandler(this.LoginFrom_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button Btn_login;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txt_user;
		private System.Windows.Forms.TextBox txt_passward;
		private System.Windows.Forms.LinkLabel Lkbl_Signin;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button Btn_out;
	}
}