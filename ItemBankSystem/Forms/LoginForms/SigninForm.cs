﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;

namespace ItemBankSystem.Forms.LoginForms
{
	public partial class SigninForm : Form
	{
		public SigninForm()
		{
			InitializeComponent();
		}
		//返回上一窗体
		private void Btn_return_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		//注册按钮
		private void Btn_Signin_Click(object sender, EventArgs e)
		{
			string txt_User = txt_user.Text;
			string txt_Passward = txt_passward.Text;

			if (txt_Passward == "" || txt_User == "")
			{
				MessageBox.Show("账号或者密码不能为空", "警告", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			else
			{
				try
				{
					SqlServerEmploy sqlServer = new SqlServerEmploy();
					SqlDataReader sqlData = sqlServer.GetSqlDataReader("select * from tb_Users where User_id='" + txt_User + "'");
					if (sqlData.HasRows)
					{
						MessageBox.Show("该账户已存在", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
						return;
					}
					sqlData.Close();
					if (sqlServer.Getsqlcom("insert into tb_Users(User_id,User_passward,User_type) values('" + txt_User + "','" + txt_Passward + "',1)") > 0)
					{
						MessageBox.Show("账号:" + txt_User + "\n" + "注册成功", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
					}
				}
				catch (Exception)
				{
					MessageBox.Show("注册失败", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
			}
		}
	}
}
