﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;

namespace ItemBankSystem.Forms.LoginForms
{
	public partial class LoginFrom : Form
	{
		public LoginFrom()
		{
			InitializeComponent();
		}
		//关闭程序
		private void Btn_out_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}
		//注册账号
		private void Lkbl_Signin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			SigninForm signinForm = new SigninForm();
			signinForm.ShowDialog();
		}
		//登录按钮
		private void Btn_login_Click(object sender, EventArgs e)
		{
			string txt_User = txt_user.Text;
			string txt_Passward = txt_passward.Text;
			if (txt_Passward == "" || txt_User == "")
			{
				MessageBox.Show("账号或者密码不能为空", "警告", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			else
			{
				try
				{
					SqlServerEmploy sqlServer = new SqlServerEmploy();
					SqlDataReader sqlData = sqlServer.GetSqlDataReader("select * from tb_Users where User_id='" + txt_User + "'");
					sqlData.Read();
					if (!sqlData.HasRows)
					{
						MessageBox.Show("此账户不存在", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
						txt_passward.Clear();
						txt_user.Clear();
						return;
					}
					else if (sqlData["User_passward"].ToString() == txt_Passward)
					{
						PublicStaticClass.user = new UserClass(txt_User,(int)sqlData["User_type"]);
						sqlData.Close();
						MainForm mainForm = new MainForm();
						mainForm.Show();
						this.Visible = false;
					}
					else
					{
						Debug.WriteLine(sqlData["User_passward"].ToString()+"--"+txt_Passward);
						txt_passward.Clear();
						sqlData.Close();
						MessageBox.Show("密码错误", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
					}
				}
				catch
				{
					MessageBox.Show("数据库连接失败", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}
		//加载数据库登录信息
		private void LoginFrom_Load(object sender, EventArgs e)
		{
			IniFiles ini = new IniFiles();
			if (!ini.ExistINIFile())
			{
				ini.IniWriteValue("sql_server", "ip", "DESKTOP-O34J73S");
				ini.IniWriteValue("sql_server", "name", "sa");
				ini.IniWriteValue("sql_server", "passward", "wang123456");
			}
			PublicStaticClass.Sql_server_ip = ini.IniReadValue("sql_server", "ip");
			PublicStaticClass.Sql_server_name = ini.IniReadValue("sql_server", "name");
			PublicStaticClass.Sql_server_passward = ini.IniReadValue("sql_server", "passward");
		}
	}
}
