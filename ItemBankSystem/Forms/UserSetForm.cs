﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;

namespace ItemBankSystem.Forms
{
	public partial class UserSetForm : Form
	{
		public UserSetForm()
		{
			InitializeComponent();
		}

		UserClass User=new UserClass ();
		private void UserSetForm_Load(object sender, EventArgs e)
		{
			switch (PublicStaticClass.user.Type)
			{
				case 0: this.Text = "管理员配置"; break;
				case 1: this.Text = "用户配置"; groupBox1.Visible = false; break;
				default:
					break;
			}
		}
		//修改密码
		private void btn_update_Click(object sender, EventArgs e)
		{
			string txt_passward = tbx_passward.Text.Trim();
			string txt_passwardt = tbx_passwardtwo.Text.Trim();
			if (txt_passward == string.Empty || txt_passwardt == string.Empty)
			{
				MessageBox.Show("请填写密码", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			else if (txt_passward != txt_passwardt)
			{
				MessageBox.Show("两次密码不一致", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				tbx_passwardtwo.Clear();
			}
			else
			{
				SqlServerEmploy sqlServer = new SqlServerEmploy();
				if (PublicStaticClass.user.update_Passward(txt_passward))
				{
					MessageBox.Show("密码修改成功", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
					tbx_passward.Clear();
					tbx_passwardtwo.Clear();
				}
				else
				{
					MessageBox.Show("密码修改失败", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
			}
		}
		//管理员查询用户
		private void btn_select_Click(object sender, EventArgs e)
		{
			Refresh_data();
		}
		//选择用户
		private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left && e.RowIndex >= 0 && e.ColumnIndex >= 0)
			{
				dataGridView1.ClearSelection();
				dataGridView1.Rows[e.RowIndex].Selected = true;
				dataGridView1.CurrentCell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
				User = new UserClass(dataGridView1.Rows[e.RowIndex].Cells["User_id"].Value.ToString(), (int)dataGridView1.Rows[e.RowIndex].Cells["User_type"].Value);
			}
		}

		private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			dataGridView1.ClearSelection();
		}

		private void btn_delete_Click(object sender, EventArgs e)
		{
			if (User.User != string.Empty)
			{
				SqlServerEmploy sqlServer = new SqlServerEmploy();
				if (User.User == PublicStaticClass.user.User)
				{
					MessageBox.Show("禁止删除当前登录账户", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else if (User.delete_user())
				{
					Refresh_data();
				}
				else
				{
					MessageBox.Show("用户删除失败", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
			}
			else
			{
				MessageBox.Show("请先选择用户", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}

		private void btn_Settype_Click(object sender, EventArgs e)
		{
			if (User.User != string.Empty)
			{
				string type = null;
				int types = 0;
				switch (User.Type)
				{
					case 0: type = "降级为普通用户"; types = 1; break;
					case 1: type = "升级为管理员"; types = 0; break;
				}
				SqlServerEmploy sqlServer = new SqlServerEmploy();
				if (User.User == PublicStaticClass.user.User)
				{
					MessageBox.Show("禁止修改当前登录账户", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else if (User.update_type(types))
				{
					Refresh_data();
					MessageBox.Show("用户权限" + type, "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
				else
				{
					MessageBox.Show("用户权限修改失败", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				}
			}
			else
			{
				MessageBox.Show("请先选择用户", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}
		private void Refresh_data()
		{
			string user = tbx_user.Text.Trim();
			SqlServerEmploy sqlServer = new SqlServerEmploy();
			DataSet data = new DataSet();
			if (user == string.Empty)
			{
				data = sqlServer.GetDataSet("select * from tb_Users");
				dataGridView1.DataSource = data.Tables[0];
				dataGridView1.Columns["User_id"].HeaderText = "用户名";
				dataGridView1.Columns["User_passward"].HeaderText = "密码";
				dataGridView1.Columns["User_type"].HeaderText = "权限";
			}
			else
			{
				data = sqlServer.GetDataSet("select * from tb_Users where User_id='" + user + "'");
				dataGridView1.DataSource = data.Tables[0];
				dataGridView1.Columns["User_id"].HeaderText = "用户名";
				dataGridView1.Columns["User_passward"].HeaderText = "密码";
				dataGridView1.Columns["User_type"].HeaderText = "权限";
			}
		}
	}
}
