﻿namespace ItemBankSystem.Forms
{
	partial class TestQuestionForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.btn_LL = new System.Windows.Forms.Button();
			this.btn_CX = new System.Windows.Forms.Button();
			this.tbn_TJ = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.Silver;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
			this.splitContainer1.Size = new System.Drawing.Size(834, 461);
			this.splitContainer1.SplitterDistance = 156;
			this.splitContainer1.TabIndex = 1;
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
			this.flowLayoutPanel1.Controls.Add(this.btn_LL);
			this.flowLayoutPanel1.Controls.Add(this.btn_CX);
			this.flowLayoutPanel1.Controls.Add(this.tbn_TJ);
			this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(156, 461);
			this.flowLayoutPanel1.TabIndex = 0;
			// 
			// btn_LL
			// 
			this.btn_LL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_LL.Font = new System.Drawing.Font("新宋体", 10F);
			this.btn_LL.Location = new System.Drawing.Point(0, 0);
			this.btn_LL.Margin = new System.Windows.Forms.Padding(0);
			this.btn_LL.Name = "btn_LL";
			this.btn_LL.Size = new System.Drawing.Size(156, 49);
			this.btn_LL.TabIndex = 0;
			this.btn_LL.Text = "浏览试题";
			this.btn_LL.UseVisualStyleBackColor = true;
			this.btn_LL.Click += new System.EventHandler(this.btn_LL_Click);
			// 
			// btn_CX
			// 
			this.btn_CX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_CX.Font = new System.Drawing.Font("新宋体", 10F);
			this.btn_CX.Location = new System.Drawing.Point(0, 49);
			this.btn_CX.Margin = new System.Windows.Forms.Padding(0);
			this.btn_CX.Name = "btn_CX";
			this.btn_CX.Size = new System.Drawing.Size(156, 49);
			this.btn_CX.TabIndex = 1;
			this.btn_CX.Text = "查询试题";
			this.btn_CX.UseVisualStyleBackColor = true;
			this.btn_CX.Click += new System.EventHandler(this.btn_CX_Click);
			// 
			// tbn_TJ
			// 
			this.tbn_TJ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tbn_TJ.Font = new System.Drawing.Font("新宋体", 10F);
			this.tbn_TJ.Location = new System.Drawing.Point(0, 98);
			this.tbn_TJ.Margin = new System.Windows.Forms.Padding(0);
			this.tbn_TJ.Name = "tbn_TJ";
			this.tbn_TJ.Size = new System.Drawing.Size(156, 49);
			this.tbn_TJ.TabIndex = 2;
			this.tbn_TJ.Text = "添加试题";
			this.tbn_TJ.UseVisualStyleBackColor = true;
			this.tbn_TJ.Click += new System.EventHandler(this.tbn_TJ_Click);
			// 
			// TestQuestionForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(834, 461);
			this.ControlBox = false;
			this.Controls.Add(this.splitContainer1);
			this.Name = "TestQuestionForm";
			this.Text = "试题管理";
			this.Load += new System.EventHandler(this.TestQuestionForm_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.flowLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Button btn_LL;
		private System.Windows.Forms.Button btn_CX;
		private System.Windows.Forms.Button tbn_TJ;
	}
}