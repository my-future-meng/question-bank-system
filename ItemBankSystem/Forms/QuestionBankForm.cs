﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;
using ItemBankSystem.Forms.QuestionBankForms;

namespace ItemBankSystem.Forms
{
	public partial class QuestionBankForm : Form
	{
		public QuestionBankForm()
		{
			InitializeComponent();
		}
		//填空
		private void btn_TK_Click(object sender, EventArgs e)
		{
			PublicStaticClass.change_btn = this.btn_TK.Name;
			splitContainer1.Panel2.Controls.Clear();
			PublicTestQuestionForm publicTestQuestion=new PublicTestQuestionForm  
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(publicTestQuestion);
			publicTestQuestion.Show();
		}
		//选择
		private void btn_XZ_Click(object sender, EventArgs e)
		{
			PublicStaticClass.change_btn = this.btn_XZ.Name;
			splitContainer1.Panel2.Controls.Clear();
			PublicTestQuestionForm publicTestQuestion = new PublicTestQuestionForm
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(publicTestQuestion);
			publicTestQuestion.Show();
		}
		//编程填空
		private void btn_BCTK_Click(object sender, EventArgs e)
		{
			PublicStaticClass.change_btn = this.btn_BCTK.Name;
			splitContainer1.Panel2.Controls.Clear();
			PublicTestQuestionForm publicTestQuestion = new PublicTestQuestionForm
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(publicTestQuestion);
			publicTestQuestion.Show();
		}
		//编程改错
		private void btn_BCGC_Click(object sender, EventArgs e)
		{
			PublicStaticClass.change_btn = this.btn_BCGC.Name;
			splitContainer1.Panel2.Controls.Clear();
			PublicTestQuestionForm publicTestQuestion = new PublicTestQuestionForm
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(publicTestQuestion);
			publicTestQuestion.Show();
		}
		//编程阅读
		private void btn_BCYD_Click(object sender, EventArgs e)
		{
			PublicStaticClass.change_btn = this.btn_BCYD.Name;
			splitContainer1.Panel2.Controls.Clear();
			PublicTestQuestionForm publicTestQuestion = new PublicTestQuestionForm
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(publicTestQuestion);
			publicTestQuestion.Show();
		}
		//编程题
		private void btn_BC_Click(object sender, EventArgs e)
		{
			PublicStaticClass.change_btn = this.btn_BC.Name;
			splitContainer1.Panel2.Controls.Clear();
			PublicTestQuestionForm publicTestQuestion = new PublicTestQuestionForm
			{
				Dock = DockStyle.Fill,
				TopLevel = false,
				FormBorderStyle = FormBorderStyle.None
			};
			splitContainer1.Panel2.Controls.Add(publicTestQuestion);
			publicTestQuestion.Show();
		}
	}
}
