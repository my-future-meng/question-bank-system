﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;

namespace ItemBankSystem.Forms
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void 试题库管理ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (this.MdiChildren.Length > 0)
			{
				this.MdiChildren[0].Close();
			}
			QuestionBankForm questionBankForm = new QuestionBankForm();
			questionBankForm.MdiParent = this;
			questionBankForm.WindowState = FormWindowState.Maximized;
			questionBankForm.Show();
		}
		//退出系统
		private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			Application.Exit();
		}

		private void 试卷管理ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (this.MdiChildren.Length > 0)
			{
				this.MdiChildren[0].Close();
			}
			TestPaperForm testPaperForm = new TestPaperForm();
			testPaperForm.MdiParent = this;
			testPaperForm.WindowState = FormWindowState.Maximized;
			testPaperForm.Show();
		}

		private void 试题管理ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (this.MdiChildren.Length > 0)
			{
				this.MdiChildren[0].Close();
			}
			TestQuestionForm testQuestionForm = new TestQuestionForm();
			testQuestionForm.MdiParent = this;
			testQuestionForm.WindowState = FormWindowState.Maximized;
			testQuestionForm.Show();
		}
		//退出系统
		private void 退出系统ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}
		//用户设置
		private void 用户设置ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			UserSetForm userSet = new UserSetForm();
			userSet.ShowDialog();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			tssl_user.Text = PublicStaticClass.user.User;
			switch (PublicStaticClass.user.Type)
			{
				case 0: tssl_type.Text = "管理员"; 用户设置ToolStripMenuItem.Visible = false; break;
				case 1:tssl_type.Text = "普通用户"; 管理员设置ToolStripMenuItem.Visible = false;  break;
				default:
					break;
			}
		}
		//管理员设置
		private void 管理员设置ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			UserSetForm userSet = new UserSetForm();
			userSet.ShowDialog();
		}
	}
}
