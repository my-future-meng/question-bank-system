﻿namespace ItemBankSystem.Forms
{
	partial class QuestionBankForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.btn_TK = new System.Windows.Forms.Button();
			this.btn_XZ = new System.Windows.Forms.Button();
			this.btn_BCTK = new System.Windows.Forms.Button();
			this.btn_BCGC = new System.Windows.Forms.Button();
			this.btn_BCYD = new System.Windows.Forms.Button();
			this.btn_BC = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.Silver;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
			this.splitContainer1.Size = new System.Drawing.Size(834, 461);
			this.splitContainer1.SplitterDistance = 156;
			this.splitContainer1.TabIndex = 0;
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
			this.flowLayoutPanel1.Controls.Add(this.btn_XZ);
			this.flowLayoutPanel1.Controls.Add(this.btn_TK);
			this.flowLayoutPanel1.Controls.Add(this.btn_BCTK);
			this.flowLayoutPanel1.Controls.Add(this.btn_BCGC);
			this.flowLayoutPanel1.Controls.Add(this.btn_BCYD);
			this.flowLayoutPanel1.Controls.Add(this.btn_BC);
			this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(156, 461);
			this.flowLayoutPanel1.TabIndex = 0;
			// 
			// btn_TK
			// 
			this.btn_TK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_TK.Font = new System.Drawing.Font("新宋体", 10F);
			this.btn_TK.Location = new System.Drawing.Point(0, 49);
			this.btn_TK.Margin = new System.Windows.Forms.Padding(0);
			this.btn_TK.Name = "btn_TK";
			this.btn_TK.Size = new System.Drawing.Size(156, 49);
			this.btn_TK.TabIndex = 0;
			this.btn_TK.Text = "填空题库";
			this.btn_TK.UseVisualStyleBackColor = true;
			this.btn_TK.Click += new System.EventHandler(this.btn_TK_Click);
			// 
			// btn_XZ
			// 
			this.btn_XZ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_XZ.Font = new System.Drawing.Font("新宋体", 10F);
			this.btn_XZ.Location = new System.Drawing.Point(0, 0);
			this.btn_XZ.Margin = new System.Windows.Forms.Padding(0);
			this.btn_XZ.Name = "btn_XZ";
			this.btn_XZ.Size = new System.Drawing.Size(156, 49);
			this.btn_XZ.TabIndex = 1;
			this.btn_XZ.Text = "选择题库";
			this.btn_XZ.UseVisualStyleBackColor = true;
			this.btn_XZ.Click += new System.EventHandler(this.btn_XZ_Click);
			// 
			// btn_BCTK
			// 
			this.btn_BCTK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_BCTK.Font = new System.Drawing.Font("新宋体", 10F);
			this.btn_BCTK.Location = new System.Drawing.Point(0, 98);
			this.btn_BCTK.Margin = new System.Windows.Forms.Padding(0);
			this.btn_BCTK.Name = "btn_BCTK";
			this.btn_BCTK.Size = new System.Drawing.Size(156, 49);
			this.btn_BCTK.TabIndex = 2;
			this.btn_BCTK.Text = "编程填空题库";
			this.btn_BCTK.UseVisualStyleBackColor = true;
			this.btn_BCTK.Click += new System.EventHandler(this.btn_BCTK_Click);
			// 
			// btn_BCGC
			// 
			this.btn_BCGC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_BCGC.Font = new System.Drawing.Font("新宋体", 10F);
			this.btn_BCGC.Location = new System.Drawing.Point(0, 147);
			this.btn_BCGC.Margin = new System.Windows.Forms.Padding(0);
			this.btn_BCGC.Name = "btn_BCGC";
			this.btn_BCGC.Size = new System.Drawing.Size(156, 49);
			this.btn_BCGC.TabIndex = 3;
			this.btn_BCGC.Text = "编程改错题库";
			this.btn_BCGC.UseVisualStyleBackColor = true;
			this.btn_BCGC.Click += new System.EventHandler(this.btn_BCGC_Click);
			// 
			// btn_BCYD
			// 
			this.btn_BCYD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_BCYD.Font = new System.Drawing.Font("新宋体", 10F);
			this.btn_BCYD.Location = new System.Drawing.Point(0, 196);
			this.btn_BCYD.Margin = new System.Windows.Forms.Padding(0);
			this.btn_BCYD.Name = "btn_BCYD";
			this.btn_BCYD.Size = new System.Drawing.Size(156, 49);
			this.btn_BCYD.TabIndex = 4;
			this.btn_BCYD.Text = "编程阅读题库";
			this.btn_BCYD.UseVisualStyleBackColor = true;
			this.btn_BCYD.Click += new System.EventHandler(this.btn_BCYD_Click);
			// 
			// btn_BC
			// 
			this.btn_BC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_BC.Font = new System.Drawing.Font("新宋体", 10F);
			this.btn_BC.Location = new System.Drawing.Point(0, 245);
			this.btn_BC.Margin = new System.Windows.Forms.Padding(0);
			this.btn_BC.Name = "btn_BC";
			this.btn_BC.Size = new System.Drawing.Size(156, 49);
			this.btn_BC.TabIndex = 5;
			this.btn_BC.Text = "编程题库";
			this.btn_BC.UseVisualStyleBackColor = true;
			this.btn_BC.Click += new System.EventHandler(this.btn_BC_Click);
			// 
			// QuestionBankForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(834, 461);
			this.ControlBox = false;
			this.Controls.Add(this.splitContainer1);
			this.Name = "QuestionBankForm";
			this.Text = "QuestionBankForm";
			this.splitContainer1.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.flowLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Button btn_TK;
		private System.Windows.Forms.Button btn_XZ;
		private System.Windows.Forms.Button btn_BCTK;
		private System.Windows.Forms.Button btn_BCGC;
		private System.Windows.Forms.Button btn_BCYD;
		private System.Windows.Forms.Button btn_BC;
	}
}