﻿namespace ItemBankSystem.Forms.TestPaperForms
{
	partial class AddTestPaperForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cbx_classes = new System.Windows.Forms.ComboBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.nud_编程题 = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.nud_编程阅读题 = new System.Windows.Forms.NumericUpDown();
			this.nud_编程改错题 = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.nud_编程填空题 = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.nud_填空题 = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.nud_选择题 = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.btn_autocreate = new System.Windows.Forms.Button();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.txt_title = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nud_编程题)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_编程阅读题)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_编程改错题)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_编程填空题)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_填空题)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_选择题)).BeginInit();
			this.groupBox3.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 27);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(41, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "科目：";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cbx_classes);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(41, 131);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(170, 72);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "第二步";
			// 
			// cbx_classes
			// 
			this.cbx_classes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbx_classes.FormattingEnabled = true;
			this.cbx_classes.Items.AddRange(new object[] {
            "C语言"});
			this.cbx_classes.Location = new System.Drawing.Point(53, 24);
			this.cbx_classes.Name = "cbx_classes";
			this.cbx_classes.Size = new System.Drawing.Size(98, 20);
			this.cbx_classes.TabIndex = 1;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.nud_编程题);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.nud_编程阅读题);
			this.groupBox2.Controls.Add(this.nud_编程改错题);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.nud_编程填空题);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.nud_填空题);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.nud_选择题);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Location = new System.Drawing.Point(41, 224);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(170, 191);
			this.groupBox2.TabIndex = 2;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "第三步";
			// 
			// nud_编程题
			// 
			this.nud_编程题.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.nud_编程题.Location = new System.Drawing.Point(101, 161);
			this.nud_编程题.Name = "nud_编程题";
			this.nud_编程题.Size = new System.Drawing.Size(56, 21);
			this.nud_编程题.TabIndex = 9;
			this.nud_编程题.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 163);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(53, 12);
			this.label7.TabIndex = 8;
			this.label7.Text = "编程题：";
			// 
			// nud_编程阅读题
			// 
			this.nud_编程阅读题.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.nud_编程阅读题.Location = new System.Drawing.Point(101, 134);
			this.nud_编程阅读题.Name = "nud_编程阅读题";
			this.nud_编程阅读题.Size = new System.Drawing.Size(56, 21);
			this.nud_编程阅读题.TabIndex = 7;
			this.nud_编程阅读题.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// nud_编程改错题
			// 
			this.nud_编程改错题.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.nud_编程改错题.Location = new System.Drawing.Point(101, 107);
			this.nud_编程改错题.Name = "nud_编程改错题";
			this.nud_编程改错题.Size = new System.Drawing.Size(56, 21);
			this.nud_编程改错题.TabIndex = 7;
			this.nud_编程改错题.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 136);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(77, 12);
			this.label6.TabIndex = 6;
			this.label6.Text = "编程阅读题：";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 109);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(77, 12);
			this.label5.TabIndex = 6;
			this.label5.Text = "编程改错题：";
			// 
			// nud_编程填空题
			// 
			this.nud_编程填空题.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.nud_编程填空题.Location = new System.Drawing.Point(101, 80);
			this.nud_编程填空题.Name = "nud_编程填空题";
			this.nud_编程填空题.Size = new System.Drawing.Size(56, 21);
			this.nud_编程填空题.TabIndex = 5;
			this.nud_编程填空题.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 82);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(77, 12);
			this.label4.TabIndex = 4;
			this.label4.Text = "编程填空题：";
			// 
			// nud_填空题
			// 
			this.nud_填空题.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.nud_填空题.Location = new System.Drawing.Point(101, 53);
			this.nud_填空题.Name = "nud_填空题";
			this.nud_填空题.Size = new System.Drawing.Size(56, 21);
			this.nud_填空题.TabIndex = 3;
			this.nud_填空题.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 55);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(53, 12);
			this.label3.TabIndex = 2;
			this.label3.Text = "填空题：";
			// 
			// nud_选择题
			// 
			this.nud_选择题.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.nud_选择题.Location = new System.Drawing.Point(101, 26);
			this.nud_选择题.Name = "nud_选择题";
			this.nud_选择题.Size = new System.Drawing.Size(56, 21);
			this.nud_选择题.TabIndex = 1;
			this.nud_选择题.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 28);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(53, 12);
			this.label2.TabIndex = 0;
			this.label2.Text = "选择题：";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.btn_autocreate);
			this.groupBox3.Location = new System.Drawing.Point(41, 444);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(170, 75);
			this.groupBox3.TabIndex = 3;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "第四步";
			// 
			// btn_autocreate
			// 
			this.btn_autocreate.BackColor = System.Drawing.Color.CornflowerBlue;
			this.btn_autocreate.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.btn_autocreate.Location = new System.Drawing.Point(8, 20);
			this.btn_autocreate.Name = "btn_autocreate";
			this.btn_autocreate.Size = new System.Drawing.Size(156, 49);
			this.btn_autocreate.TabIndex = 1;
			this.btn_autocreate.Text = "自动生成试卷";
			this.btn_autocreate.UseVisualStyleBackColor = false;
			this.btn_autocreate.Click += new System.EventHandler(this.btn_autocreate_Click);
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.txt_title);
			this.groupBox6.Controls.Add(this.label8);
			this.groupBox6.Location = new System.Drawing.Point(41, 33);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(170, 72);
			this.groupBox6.TabIndex = 9;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "第一步";
			// 
			// txt_title
			// 
			this.txt_title.Location = new System.Drawing.Point(8, 42);
			this.txt_title.Name = "txt_title";
			this.txt_title.Size = new System.Drawing.Size(149, 21);
			this.txt_title.TabIndex = 1;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(6, 27);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(65, 12);
			this.label8.TabIndex = 0;
			this.label8.Text = "试卷标题：";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("宋体", 25F);
			this.label9.ForeColor = System.Drawing.Color.Red;
			this.label9.Location = new System.Drawing.Point(278, 252);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(0, 34);
			this.label9.TabIndex = 10;
			// 
			// AddTestPaperForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1069, 617);
			this.ControlBox = false;
			this.Controls.Add(this.label9);
			this.Controls.Add(this.groupBox6);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "AddTestPaperForm";
			this.Text = "AddTestPaperForm";
			this.Load += new System.EventHandler(this.AddTestPaperForm_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nud_编程题)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_编程阅读题)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_编程改错题)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_编程填空题)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_填空题)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_选择题)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			this.groupBox6.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ComboBox cbx_classes;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.NumericUpDown nud_选择题;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown nud_填空题;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown nud_编程阅读题;
		private System.Windows.Forms.NumericUpDown nud_编程改错题;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown nud_编程填空题;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown nud_编程题;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button btn_autocreate;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txt_title;
		private System.Windows.Forms.Label label9;
	}
}