﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;
using ItemBankSystem.Forms.PublicForms;

namespace ItemBankSystem.Forms.TestPaperForms
{
	public partial class LookTestPaperForm : Form
	{
		public LookTestPaperForm()
		{
			InitializeComponent();
		}

		private void LookTestPaperForm_Load(object sender, EventArgs e)
		{
			Refresh_data();
		}
		/// <summary>
		/// 刷新试卷列表
		/// </summary>
		public void Refresh_data()
		{
			SqlServerEmploy sqlServerEmploy = new SqlServerEmploy();
			DataSet dataSet = new DataSet();
			dataSet = sqlServerEmploy.GetDataSet(
				"select tb_Test_Paper.paper_id,tb_Test_Paper.paper_title,tb_Classes.classes from tb_Test_Paper,tb_Classes where tb_Test_Paper.classes_id=tb_Classes.classes_id"
				);
			dataGridView1.DataSource = dataSet.Tables[0];
			dataGridView1.Columns["paper_id"].HeaderText = "试卷编号";
			dataGridView1.Columns["paper_title"].HeaderText = "试卷标题";
			dataGridView1.Columns["classes"].HeaderText = "课程";
		}
		//查看试卷
		private void 查看ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OperateTestPaperForm operateTestPaper = new OperateTestPaperForm();
			operateTestPaper.ShowDialog();
		}
		/// <summary>
		/// 选择试卷
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right && e.RowIndex >= 0 && e.ColumnIndex >= 0)
			{
				dataGridView1.ClearSelection();
				dataGridView1.Rows[e.RowIndex].Selected = true;
				dataGridView1.CurrentCell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
				contextMenuStrip1.Show(MousePosition.X, MousePosition.Y);
				PublicStaticClass.TestPaper = new TestPaperClass((int)dataGridView1.Rows[e.RowIndex].Cells["paper_id"].Value,
					dataGridView1.Rows[e.RowIndex].Cells["classes"].Value.ToString(),
					dataGridView1.Rows[e.RowIndex].Cells["paper_title"].Value.ToString());
			}
		}
		//删除试卷
		private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if(PublicStaticClass.TestPaper.Paper_Delete())
			{
				dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
			}
			else
			{
				MessageBox.Show("试卷删除失败", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}

		private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			dataGridView1.ClearSelection();
		}
	}
}
