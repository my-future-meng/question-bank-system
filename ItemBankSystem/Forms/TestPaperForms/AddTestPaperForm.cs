﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;

namespace ItemBankSystem.Forms.TestPaperForms
{
	public partial class AddTestPaperForm : Form
	{
		public AddTestPaperForm()
		{
			InitializeComponent();
		}

		//试卷自动生成
		private void btn_autocreate_Click(object sender, EventArgs e)
		{
			List<int> question_id = new List<int>();

			string title = txt_title.Text;
			string classes = cbx_classes.Text;
			int xuanze = (int)nud_选择题.Value;
			int tiankong = (int)nud_填空题.Value;
			int bianchengtiankong = (int)nud_编程填空题.Value;
			int bianchengyuedu = (int)nud_编程阅读题.Value;
			int bianchenggaicuo = (int)nud_编程改错题.Value;
			int bianchengti = (int)nud_编程题.Value;
			if (title == string.Empty)
			{
				MessageBox.Show("试卷标题不能为空", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			else if ((tiankong == 0) && (xuanze == 0) && (bianchenggaicuo == 0) && (bianchengtiankong == 0) && (bianchengyuedu == 0))
			{
				MessageBox.Show("请选择题目数量", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			else if (classes == string.Empty)
			{
				MessageBox.Show("请选择科目", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			else
			{
				label9.Text = "试卷自动生成中。。。";
				if (xuanze != 0)
				{
					question_id.AddRange(rand_quesitonid(xuanze, "选择题"));
				}
				if (tiankong != 0)
				{
					question_id.AddRange(rand_quesitonid(tiankong, "填空题"));
				}
				if (bianchengtiankong != 0)
				{
					question_id.AddRange(rand_quesitonid(bianchengtiankong, "编程填空题"));
				}
				if (bianchengyuedu != 0)
				{
					question_id.AddRange(rand_quesitonid(bianchengyuedu, "编程阅读题"));
				}
				if (bianchenggaicuo != 0)
				{
					question_id.AddRange(rand_quesitonid(bianchenggaicuo, "编程改错题"));
				}
				if (bianchengti != 0)
				{
					question_id.AddRange(rand_quesitonid(bianchengti, "编程题"));
				}
				foreach (var item in question_id)
				{
					Debug.Write(item.ToString());
				}
				DataSet data = new DataSet();
				SqlServerEmploy sqlServer = new SqlServerEmploy();
				try
				{
					data = sqlServer.GetDataSet("insert into tb_Test_Paper(paper_title, classes_id) " +
						"values('" + title + "', (select tb_Classes.classes_id from tb_Classes where classes = '" + classes + "')); " +
						"select @@IDENTITY");
					foreach (var item in question_id)
					{
						sqlServer.Getsqlcom("insert into tb_Test_Paper_Single(paper_id,question_id) values(" + data.Tables[0].Rows[0][0] + "," + item + ")");
					}
				}
				catch
				{
					label9.Text = "试卷生成失败，请重试";
					return;
				}
				label9.Text = "试卷生成成功，请点击左侧浏览试卷选项进行查看";
			}
		}
		/// <summary>
		/// 生成随机的试题的编号
		/// </summary>
		/// <param name="questions">需要试题的数量</param>
		/// <param name="type">试题类型</param>
		/// <returns>随机试题编号列表</returns>
		private List<int> rand_quesitonid(int questions, string type)
		{
			int a;
			SqlServerEmploy sqlServer = new SqlServerEmploy();
			Random random = new Random();
			DataSet data = new DataSet();
			List<int> questionid = new List<int>();
			List<int> rand = new List<int>();
			data = sqlServer.GetDataSet("select question_id from tb_Question_Library where question_type='" + type + "'");
			for (int i = 0; i < questions; i++)
			{
				a = random.Next(data.Tables[0].Rows.Count);
				if (rand.Exists(p => p == a))
				{
					i--;
				}
				else
				{
					rand.Add(a);
					questionid.Add((int)data.Tables[0].Rows[a][0]);
				}
			}
			return questionid;
		}
		//查询数据库各种信息
		private void AddTestPaperForm_Load(object sender, EventArgs e)
		{
			SqlServerEmploy sqlServer = new SqlServerEmploy();
			Dictionary<NumericUpDown, int> max = new Dictionary<NumericUpDown, int>();
			foreach (var item in groupBox2.Controls)
			{
				if (item is NumericUpDown upDown)
				{
					max.Add(upDown, (int)sqlServer.GetDataSet("select COUNT(*) from tb_Question_Library where question_type='" + upDown.Name.Replace("nud_", "") + "'").Tables[0].Rows[0][0]);
				}
			}
			foreach (var item in max)
			{
				item.Key.Maximum = item.Value;
			}
		}
	}
}

