﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;
using ItemBankSystem.Forms.PublicForms;

namespace ItemBankSystem.Forms.TestQuestionForms
{
	public partial class FindTestQuestionForm : Form
	{
		public FindTestQuestionForm()
		{
			InitializeComponent();
		}
		List<string> strs = new List<string>();
		//查询按钮
		private void btn_select_Click(object sender, EventArgs e)
		{
			string question_id = tbx_questioin_id.Text.Trim();
			string classes = cbx_classes.Text.Trim();
			string type = cbx_type.Text.Trim();
			string datetime = tbx_date.Text.Trim();
			if (question_id + classes + type + datetime == "")
			{
				MessageBox.Show("至少添加一项关键词", "软件警告", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			else
			{
				try
				{
					if (question_id != "")
					{
						question_id = "tb_Question_Library.question_id = " + question_id;
						strs.Add(question_id);
					}
					if (classes != "")
					{
						classes = "tb_Classes.classes = '" + classes + "'";
						strs.Add(classes);
					}
					if (type != "")
					{
						type = "tb_Question_Library.question_type = '" + type + "'";
						strs.Add(type);
					}
					if (datetime != "")
					{
						datetime = "tb_Question_Library.question_time = " + datetime.Replace("-", "");
						strs.Add(datetime);
					}
					Refresh_data();
				}
				catch
				{
					MessageBox.Show("查询失败!，关键词添加错误!", "软件警告", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
					strs.Clear();
				}
			}
		}
		//清空
		private void btn_clear_Click(object sender, EventArgs e)
		{
			tbx_questioin_id.Clear();
			tbx_date.Clear();
			cbx_classes.SelectedIndex = 0;
			cbx_type.SelectedIndex = 0;
		}
		//选择日期
		private void btn_date_Click(object sender, EventArgs e)
		{
			DateTimeForm dateTime = new DateTimeForm();
			dateTime.Location = MousePosition;
			dateTime.ShowDialog();
		}
		//右键菜单
		private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right && e.RowIndex >= 0 && e.ColumnIndex >= 0)
			{
				dataGridView1.ClearSelection();
				dataGridView1.Rows[e.RowIndex].Selected = true;
				dataGridView1.CurrentCell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
				contextMenuStrip1.Show(MousePosition.X, MousePosition.Y);
				PublicStaticClass.TestQuestion = new TestQuestionClass((int)dataGridView1.Rows[e.RowIndex].Cells["question_id"].Value,
					dataGridView1.Rows[e.RowIndex].Cells["classes"].Value.ToString(),
					dataGridView1.Rows[e.RowIndex].Cells["question_test"].Value.ToString(),
					dataGridView1.Rows[e.RowIndex].Cells["question_answer"].Value.ToString(),
					dataGridView1.Rows[e.RowIndex].Cells["question_type"].Value.ToString(),
					(int)dataGridView1.Rows[e.RowIndex].Cells["question_time"].Value);
				PublicStaticClass.Form_name = this.Name;
			}
		}

		private void 查看ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			LookTestSingleForm lookTestSingle = new LookTestSingleForm();
			lookTestSingle.ShowDialog();
		}

		private void 修改ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			UpdateTestSingleForm updateTestSingle = new UpdateTestSingleForm();
			updateTestSingle.ShowDialog();
		}

		private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (PublicStaticClass.TestQuestion.Test_Delete())
			{
				dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
			}
			else
			{
				MessageBox.Show("试题删除失败！", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}
		public void Refresh_data()
		{
			SqlServerEmploy sqlServerEmploy = new SqlServerEmploy();
			DataSet dataSet = new DataSet();
			dataSet = sqlServerEmploy.GetDataSet("select tb_Question_Library.question_id,tb_Classes.classes,tb_Question_Library.question_type,tb_Question_Library.question_test,tb_Question_Library.question_answer,tb_Question_Library.question_time " +
				"from tb_Question_Library left join tb_Classes on tb_Question_Library.classes_id=tb_Classes.classes_id " +
				"where " + PublicStaticClass.TransForms(strs, " and "));
			dataGridView1.DataSource = dataSet.Tables[0];
			dataGridView1.Columns["question_id"].HeaderText = "试题编号";
			dataGridView1.Columns["question_test"].HeaderText = "试题内容";
			dataGridView1.Columns["question_answer"].HeaderText = "试题答案";
			dataGridView1.Columns["question_type"].HeaderText = "试题类型";
			dataGridView1.Columns["question_time"].HeaderText = "添加时间";
			dataGridView1.Columns["classes"].HeaderText = "试题科目";
			strs.Clear();
		}

		private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			dataGridView1.ClearSelection();
		}

		private void FindTestQuestionForm_Load(object sender, EventArgs e)
		{
			if (PublicStaticClass.user.Type == 1)
			{
				修改ToolStripMenuItem.Enabled = false;
				删除ToolStripMenuItem.Enabled = false;
			}
		}
	}
}
