﻿namespace ItemBankSystem.Forms.TestQuestionForms
{
	partial class AddTestQuestionForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.rtbx_test_answer = new System.Windows.Forms.RichTextBox();
			this.rtbx_test_title = new System.Windows.Forms.RichTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.cbx_type = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.cbx_classes = new System.Windows.Forms.ComboBox();
			this.btn_clear = new System.Windows.Forms.Button();
			this.btn_addtest = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.rtbx_test_answer);
			this.groupBox1.Controls.Add(this.rtbx_test_title);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.cbx_type);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.cbx_classes);
			this.groupBox1.Controls.Add(this.btn_clear);
			this.groupBox1.Controls.Add(this.btn_addtest);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(929, 599);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "添加试题";
			// 
			// rtbx_test_answer
			// 
			this.rtbx_test_answer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtbx_test_answer.Location = new System.Drawing.Point(17, 345);
			this.rtbx_test_answer.Name = "rtbx_test_answer";
			this.rtbx_test_answer.Size = new System.Drawing.Size(906, 201);
			this.rtbx_test_answer.TabIndex = 19;
			this.rtbx_test_answer.Text = "";
			// 
			// rtbx_test_title
			// 
			this.rtbx_test_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtbx_test_title.Location = new System.Drawing.Point(17, 126);
			this.rtbx_test_title.Name = "rtbx_test_title";
			this.rtbx_test_title.Size = new System.Drawing.Size(906, 201);
			this.rtbx_test_title.TabIndex = 18;
			this.rtbx_test_title.Text = "";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(15, 330);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(65, 12);
			this.label3.TabIndex = 17;
			this.label3.Text = "试题答案：";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(15, 111);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(65, 12);
			this.label2.TabIndex = 16;
			this.label2.Text = "试题内容：";
			// 
			// cbx_type
			// 
			this.cbx_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbx_type.FormattingEnabled = true;
			this.cbx_type.Items.AddRange(new object[] {
            "选择题",
            "填空题",
            "编程填空题",
            "编程改错题",
            "编程阅读题",
            "编程题"});
			this.cbx_type.Location = new System.Drawing.Point(259, 20);
			this.cbx_type.Name = "cbx_type";
			this.cbx_type.Size = new System.Drawing.Size(111, 20);
			this.cbx_type.TabIndex = 13;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(188, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(65, 12);
			this.label4.TabIndex = 15;
			this.label4.Text = "试题类型：";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(15, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(41, 12);
			this.label1.TabIndex = 14;
			this.label1.Text = "课程：";
			// 
			// cbx_classes
			// 
			this.cbx_classes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbx_classes.FormattingEnabled = true;
			this.cbx_classes.Items.AddRange(new object[] {
            "C语言"});
			this.cbx_classes.Location = new System.Drawing.Point(62, 20);
			this.cbx_classes.Name = "cbx_classes";
			this.cbx_classes.Size = new System.Drawing.Size(111, 20);
			this.cbx_classes.TabIndex = 12;
			// 
			// btn_clear
			// 
			this.btn_clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_clear.BackColor = System.Drawing.Color.CornflowerBlue;
			this.btn_clear.ForeColor = System.Drawing.Color.White;
			this.btn_clear.Location = new System.Drawing.Point(769, 552);
			this.btn_clear.Name = "btn_clear";
			this.btn_clear.Size = new System.Drawing.Size(154, 41);
			this.btn_clear.TabIndex = 1;
			this.btn_clear.Text = "清空";
			this.btn_clear.UseVisualStyleBackColor = false;
			this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
			// 
			// btn_addtest
			// 
			this.btn_addtest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_addtest.BackColor = System.Drawing.Color.CornflowerBlue;
			this.btn_addtest.ForeColor = System.Drawing.Color.White;
			this.btn_addtest.Location = new System.Drawing.Point(609, 552);
			this.btn_addtest.Name = "btn_addtest";
			this.btn_addtest.Size = new System.Drawing.Size(154, 41);
			this.btn_addtest.TabIndex = 0;
			this.btn_addtest.Text = "添加试题";
			this.btn_addtest.UseVisualStyleBackColor = false;
			this.btn_addtest.Click += new System.EventHandler(this.btn_addtest_Click);
			// 
			// AddTestQuestionForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(949, 623);
			this.ControlBox = false;
			this.Controls.Add(this.groupBox1);
			this.Name = "AddTestQuestionForm";
			this.Text = "AddTestQuestionForm";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btn_clear;
		private System.Windows.Forms.Button btn_addtest;
		private System.Windows.Forms.ComboBox cbx_type;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbx_classes;
		private System.Windows.Forms.RichTextBox rtbx_test_answer;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.RichTextBox rtbx_test_title;
	}
}