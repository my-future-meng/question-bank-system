﻿namespace ItemBankSystem.Forms.TestQuestionForms
{
	partial class LookTestForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.查看ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.修改试题ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.删除试题ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.AllowUserToOrderColumns = true;
			this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView1.Location = new System.Drawing.Point(0, 0);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.RowTemplate.Height = 23;
			this.dataGridView1.Size = new System.Drawing.Size(800, 450);
			this.dataGridView1.TabIndex = 0;
			this.dataGridView1.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseUp);
			this.dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView1_DataBindingComplete);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查看ToolStripMenuItem,
            this.修改试题ToolStripMenuItem,
            this.删除试题ToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(101, 70);
			// 
			// 查看ToolStripMenuItem
			// 
			this.查看ToolStripMenuItem.Name = "查看ToolStripMenuItem";
			this.查看ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
			this.查看ToolStripMenuItem.Text = "查看";
			this.查看ToolStripMenuItem.Click += new System.EventHandler(this.查看ToolStripMenuItem_Click);
			// 
			// 修改试题ToolStripMenuItem
			// 
			this.修改试题ToolStripMenuItem.Name = "修改试题ToolStripMenuItem";
			this.修改试题ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
			this.修改试题ToolStripMenuItem.Text = "修改";
			this.修改试题ToolStripMenuItem.Click += new System.EventHandler(this.修改试题ToolStripMenuItem_Click);
			// 
			// 删除试题ToolStripMenuItem
			// 
			this.删除试题ToolStripMenuItem.Name = "删除试题ToolStripMenuItem";
			this.删除试题ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
			this.删除试题ToolStripMenuItem.Text = "删除";
			this.删除试题ToolStripMenuItem.Click += new System.EventHandler(this.删除试题ToolStripMenuItem_Click);
			// 
			// LookTestForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.dataGridView1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "LookTestForm";
			this.Text = "LookTestForm";
			this.Load += new System.EventHandler(this.LookTestForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		public System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem 修改试题ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 删除试题ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 查看ToolStripMenuItem;
	}
}