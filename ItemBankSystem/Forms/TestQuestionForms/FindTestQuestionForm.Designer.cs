﻿namespace ItemBankSystem.Forms.TestQuestionForms
{
	partial class FindTestQuestionForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btn_select = new System.Windows.Forms.Button();
			this.btn_clear = new System.Windows.Forms.Button();
			this.cbx_classes = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cbx_type = new System.Windows.Forms.ComboBox();
			this.tbx_date = new System.Windows.Forms.TextBox();
			this.btn_date = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.tbx_questioin_id = new System.Windows.Forms.TextBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.查看ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.修改ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btn_select
			// 
			this.btn_select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_select.BackColor = System.Drawing.Color.CornflowerBlue;
			this.btn_select.ForeColor = System.Drawing.Color.White;
			this.btn_select.Location = new System.Drawing.Point(767, 60);
			this.btn_select.Name = "btn_select";
			this.btn_select.Size = new System.Drawing.Size(106, 29);
			this.btn_select.TabIndex = 1;
			this.btn_select.Text = "查询试题";
			this.btn_select.UseVisualStyleBackColor = false;
			this.btn_select.Click += new System.EventHandler(this.btn_select_Click);
			// 
			// btn_clear
			// 
			this.btn_clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_clear.BackColor = System.Drawing.Color.CornflowerBlue;
			this.btn_clear.ForeColor = System.Drawing.Color.White;
			this.btn_clear.Location = new System.Drawing.Point(879, 60);
			this.btn_clear.Name = "btn_clear";
			this.btn_clear.Size = new System.Drawing.Size(106, 29);
			this.btn_clear.TabIndex = 2;
			this.btn_clear.Text = "清空查询";
			this.btn_clear.UseVisualStyleBackColor = false;
			this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
			// 
			// cbx_classes
			// 
			this.cbx_classes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbx_classes.FormattingEnabled = true;
			this.cbx_classes.Items.AddRange(new object[] {
            " ",
            "C语言"});
			this.cbx_classes.Location = new System.Drawing.Point(243, 20);
			this.cbx_classes.Name = "cbx_classes";
			this.cbx_classes.Size = new System.Drawing.Size(111, 20);
			this.cbx_classes.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(10, 23);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(65, 12);
			this.label2.TabIndex = 5;
			this.label2.Text = "试题编号：";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.cbx_type);
			this.groupBox1.Controls.Add(this.tbx_date);
			this.groupBox1.Controls.Add(this.btn_date);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.btn_clear);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.btn_select);
			this.groupBox1.Controls.Add(this.cbx_classes);
			this.groupBox1.Controls.Add(this.tbx_questioin_id);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(990, 95);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "查询关键词：";
			// 
			// cbx_type
			// 
			this.cbx_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbx_type.FormattingEnabled = true;
			this.cbx_type.Items.AddRange(new object[] {
            " ",
            "选择题",
            "填空题",
            "编程填空题",
            "编程改错题",
            "编程阅读题",
            "编程题"});
			this.cbx_type.Location = new System.Drawing.Point(440, 20);
			this.cbx_type.Name = "cbx_type";
			this.cbx_type.Size = new System.Drawing.Size(111, 20);
			this.cbx_type.TabIndex = 15;
			// 
			// tbx_date
			// 
			this.tbx_date.Location = new System.Drawing.Point(631, 21);
			this.tbx_date.Name = "tbx_date";
			this.tbx_date.ReadOnly = true;
			this.tbx_date.Size = new System.Drawing.Size(100, 21);
			this.tbx_date.TabIndex = 13;
			// 
			// btn_date
			// 
			this.btn_date.Location = new System.Drawing.Point(737, 20);
			this.btn_date.Name = "btn_date";
			this.btn_date.Size = new System.Drawing.Size(88, 23);
			this.btn_date.TabIndex = 10;
			this.btn_date.Text = "选择日期";
			this.btn_date.UseVisualStyleBackColor = true;
			this.btn_date.Click += new System.EventHandler(this.btn_date_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(560, 24);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(65, 12);
			this.label5.TabIndex = 12;
			this.label5.Text = "添加日期：";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(369, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(65, 12);
			this.label4.TabIndex = 11;
			this.label4.Text = "试题类型：";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(196, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(41, 12);
			this.label1.TabIndex = 10;
			this.label1.Text = "科目：";
			// 
			// tbx_questioin_id
			// 
			this.tbx_questioin_id.Location = new System.Drawing.Point(81, 20);
			this.tbx_questioin_id.Name = "tbx_questioin_id";
			this.tbx_questioin_id.Size = new System.Drawing.Size(100, 21);
			this.tbx_questioin_id.TabIndex = 9;
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dataGridView1.Location = new System.Drawing.Point(12, 113);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.RowHeadersWidth = 20;
			this.dataGridView1.RowTemplate.Height = 23;
			this.dataGridView1.Size = new System.Drawing.Size(990, 350);
			this.dataGridView1.TabIndex = 9;
			this.dataGridView1.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseUp);
			this.dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView1_DataBindingComplete);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查看ToolStripMenuItem,
            this.修改ToolStripMenuItem,
            this.删除ToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(101, 70);
			// 
			// 查看ToolStripMenuItem
			// 
			this.查看ToolStripMenuItem.Name = "查看ToolStripMenuItem";
			this.查看ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
			this.查看ToolStripMenuItem.Text = "查看";
			this.查看ToolStripMenuItem.Click += new System.EventHandler(this.查看ToolStripMenuItem_Click);
			// 
			// 修改ToolStripMenuItem
			// 
			this.修改ToolStripMenuItem.Name = "修改ToolStripMenuItem";
			this.修改ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
			this.修改ToolStripMenuItem.Text = "修改";
			this.修改ToolStripMenuItem.Click += new System.EventHandler(this.修改ToolStripMenuItem_Click);
			// 
			// 删除ToolStripMenuItem
			// 
			this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
			this.删除ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
			this.删除ToolStripMenuItem.Text = "删除";
			this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
			// 
			// FindTestQuestionForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1014, 475);
			this.ControlBox = false;
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.groupBox1);
			this.Name = "FindTestQuestionForm";
			this.Text = "FindTestQuestionForm";
			this.Load += new System.EventHandler(this.FindTestQuestionForm_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button btn_select;
		private System.Windows.Forms.Button btn_clear;
		private System.Windows.Forms.ComboBox cbx_classes;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox tbx_questioin_id;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label4;
		public System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button btn_date;
		public System.Windows.Forms.TextBox tbx_date;
		private System.Windows.Forms.ComboBox cbx_type;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem 修改ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem 查看ToolStripMenuItem;
	}
}