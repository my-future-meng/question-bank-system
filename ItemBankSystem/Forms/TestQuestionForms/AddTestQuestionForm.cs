﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;

namespace ItemBankSystem.Forms.TestQuestionForms
{
	public partial class AddTestQuestionForm : Form
	{
		public AddTestQuestionForm()
		{
			InitializeComponent();
		}
		//添加试题
		private void btn_addtest_Click(object sender, EventArgs e)
		{
			string classes = cbx_classes.Text.Trim();
			string type = cbx_type.Text.Trim();
			string title = rtbx_test_title.Text.Trim();
			string answer = rtbx_test_answer.Text.Trim();

			string time = DateTime.Now.ToString("yyyy-MM-dd");
			time = time.Replace("-", "");

			if (classes == "" || type == "" || title == "" || answer == "")
			{
				MessageBox.Show("课程,类型,试题内容,试题答案禁止为空！", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				SqlServerEmploy sqlServer = new SqlServerEmploy();
				DataSet dataSet = new DataSet();
				string classes_id = null;
				dataSet = sqlServer.GetDataSet("select tb_Classes.classes_id from tb_Classes where classes='" + classes + "'");
				classes_id = dataSet.Tables[0].Rows[0][0].ToString();
				int change = sqlServer.Getsqlcom("insert into " +
					"tb_Question_Library(classes_id,question_test,question_answer,question_type,question_time) " +
					"values ("+classes_id+",'"+title+"','"+answer+"','"+type+"','"+time+"')");
				if (change > 0)
				{
					MessageBox.Show("试题添加成功！", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
					rtbx_test_title.Clear();
					rtbx_test_answer.Clear();
				}
			}
		}
		//清空
		private void btn_clear_Click(object sender, EventArgs e)
		{
			rtbx_test_title.Clear();
			rtbx_test_answer.Clear();
		}
	}
}
