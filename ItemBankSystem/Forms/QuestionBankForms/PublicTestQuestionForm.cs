﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;

namespace ItemBankSystem.Forms.QuestionBankForms
{
	public partial class PublicTestQuestionForm : Form
	{
		public PublicTestQuestionForm()
		{
			InitializeComponent();
		}

		private void PublicTestQuestionForm_Load(object sender, EventArgs e)
		{
			string name_str = PublicStaticClass.change_btn;
			string sql_str = null;
			switch (name_str)
			{
				case "btn_XZ": sql_str = "选择题"; break;
				case "btn_TK": sql_str = "填空题"; break;
				case "btn_BCTK": sql_str = "编程填空题"; break;
				case "btn_BCGC": sql_str = "编程改错题"; break;
				case "btn_BCYD": sql_str = "编程阅读题"; break;
				case "btn_BC": sql_str = "编程题"; break;
				default:
					break;
			}
			SqlServerEmploy sqlServerEmploy = new SqlServerEmploy();
			DataSet dataSet = new DataSet();
			dataSet = sqlServerEmploy.GetDataSet(
				"select tb_Question_Library.question_id, tb_Classes.classes, tb_Question_Library.question_type, tb_Question_Library.question_test, tb_Question_Library.question_answer, tb_Question_Library.question_time from tb_Question_Library left join tb_Classes on tb_Question_Library.classes_id = tb_Classes.classes_id where question_type='" + sql_str + "'"
				);
			dataGridView1.DataSource = dataSet.Tables[0];
			dataGridView1.Columns["question_id"].HeaderText = "试题编号";
			dataGridView1.Columns["question_test"].HeaderText = "试题内容";
			dataGridView1.Columns["question_answer"].HeaderText = "试题答案";
			dataGridView1.Columns["classes"].HeaderText = "课程";
			dataGridView1.Columns["question_type"].HeaderText = "试题类型";
			dataGridView1.Columns["question_time"].HeaderText = "添加时间";
		}

		private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			dataGridView1.ClearSelection();
		}
	}
}
