﻿namespace ItemBankSystem.Forms
{
	partial class UserSetForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btn_Settype = new System.Windows.Forms.Button();
			this.btn_delete = new System.Windows.Forms.Button();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.btn_select = new System.Windows.Forms.Button();
			this.tbx_user = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.tbx_passwardtwo = new System.Windows.Forms.TextBox();
			this.tbx_passward = new System.Windows.Forms.TextBox();
			this.btn_update = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.btn_Settype);
			this.groupBox1.Controls.Add(this.btn_delete);
			this.groupBox1.Controls.Add(this.dataGridView1);
			this.groupBox1.Controls.Add(this.btn_select);
			this.groupBox1.Controls.Add(this.tbx_user);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Location = new System.Drawing.Point(12, 104);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(292, 201);
			this.groupBox1.TabIndex = 16;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "查找用户";
			// 
			// btn_Settype
			// 
			this.btn_Settype.Location = new System.Drawing.Point(7, 160);
			this.btn_Settype.Name = "btn_Settype";
			this.btn_Settype.Size = new System.Drawing.Size(105, 31);
			this.btn_Settype.TabIndex = 14;
			this.btn_Settype.Text = "更改权限";
			this.btn_Settype.UseVisualStyleBackColor = true;
			this.btn_Settype.Click += new System.EventHandler(this.btn_Settype_Click);
			// 
			// btn_delete
			// 
			this.btn_delete.Location = new System.Drawing.Point(181, 160);
			this.btn_delete.Name = "btn_delete";
			this.btn_delete.Size = new System.Drawing.Size(105, 31);
			this.btn_delete.TabIndex = 13;
			this.btn_delete.Text = "删除用户";
			this.btn_delete.UseVisualStyleBackColor = true;
			this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(7, 45);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.RowTemplate.Height = 23;
			this.dataGridView1.Size = new System.Drawing.Size(279, 109);
			this.dataGridView1.TabIndex = 12;
			this.dataGridView1.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseUp);
			this.dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView1_DataBindingComplete);
			// 
			// btn_select
			// 
			this.btn_select.Location = new System.Drawing.Point(203, 12);
			this.btn_select.Name = "btn_select";
			this.btn_select.Size = new System.Drawing.Size(83, 30);
			this.btn_select.TabIndex = 11;
			this.btn_select.Text = "查询";
			this.btn_select.UseVisualStyleBackColor = true;
			this.btn_select.Click += new System.EventHandler(this.btn_select_Click);
			// 
			// tbx_user
			// 
			this.tbx_user.Location = new System.Drawing.Point(69, 18);
			this.tbx_user.Name = "tbx_user";
			this.tbx_user.Size = new System.Drawing.Size(128, 21);
			this.tbx_user.TabIndex = 10;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(10, 21);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(53, 12);
			this.label5.TabIndex = 0;
			this.label5.Text = "用户名：";
			// 
			// tbx_passwardtwo
			// 
			this.tbx_passwardtwo.Location = new System.Drawing.Point(92, 43);
			this.tbx_passwardtwo.Name = "tbx_passwardtwo";
			this.tbx_passwardtwo.Size = new System.Drawing.Size(140, 21);
			this.tbx_passwardtwo.TabIndex = 15;
			// 
			// tbx_passward
			// 
			this.tbx_passward.Location = new System.Drawing.Point(92, 12);
			this.tbx_passward.Name = "tbx_passward";
			this.tbx_passward.Size = new System.Drawing.Size(140, 21);
			this.tbx_passward.TabIndex = 14;
			// 
			// btn_update
			// 
			this.btn_update.Location = new System.Drawing.Point(92, 70);
			this.btn_update.Name = "btn_update";
			this.btn_update.Size = new System.Drawing.Size(140, 28);
			this.btn_update.TabIndex = 13;
			this.btn_update.Text = "修改密码";
			this.btn_update.UseVisualStyleBackColor = true;
			this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(21, 46);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(65, 12);
			this.label3.TabIndex = 12;
			this.label3.Text = "确认密码：";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(33, 15);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(53, 12);
			this.label4.TabIndex = 11;
			this.label4.Text = "新密码：";
			// 
			// UserSetForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(316, 312);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.tbx_passwardtwo);
			this.Controls.Add(this.tbx_passward);
			this.Controls.Add(this.btn_update);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label4);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "UserSetForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Load += new System.EventHandler(this.UserSetForm_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Button btn_select;
		private System.Windows.Forms.TextBox tbx_user;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbx_passwardtwo;
		private System.Windows.Forms.TextBox tbx_passward;
		private System.Windows.Forms.Button btn_update;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button btn_Settype;
		private System.Windows.Forms.Button btn_delete;
	}
}