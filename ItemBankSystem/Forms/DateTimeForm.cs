﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;
using ItemBankSystem.Forms.QuestionBankForms;

namespace ItemBankSystem.Forms
{
	public partial class DateTimeForm : Form
	{
		public DateTimeForm()
		{
			InitializeComponent();
		}
		private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
		{
			PublicStaticClass.FindTestQuestion.tbx_date.Text = e.Start.ToString("yyyy-MM-dd");
			this.Close();
		}
	}
}
