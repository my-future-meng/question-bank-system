﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;


namespace ItemBankSystem.Forms.PublicForms
{
	public partial class OperateTestPaperForm : Form
	{
		public OperateTestPaperForm()
		{
			InitializeComponent();
		}
		//返回
		private void button2_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void OperateTestPaperForm_Load(object sender, EventArgs e)
		{
			List<int> question_id = new List<int>();//试题ID
			int paper_test_id = 0;//题目编号
			string question_type = null;
			StringBuilder question_test = new StringBuilder();
			StringBuilder question_answer = new StringBuilder();
			//搜索此试卷对应的试题编号
			SqlServerEmploy sqlServer = new SqlServerEmploy();
			DataSet dataSet = new DataSet();
			dataSet = sqlServer.GetDataSet("select tb_Test_Paper_Single.question_id from tb_Test_Paper_Single where tb_Test_Paper_Single.paper_id=" + PublicStaticClass.TestPaper.Paperid);
			for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
			{
				question_id.Add((int)dataSet.Tables[0].Rows[i]["question_id"]);
			}
			//连续查询试题内容和答案
			foreach (int item in question_id)
			{
				if (paper_test_id == 0)
				{
					question_test.Append(PublicStaticClass.TestPaper.Papertitle + "\n\n");
					question_answer.Append(PublicStaticClass.TestPaper.Papertitle + "试卷标准答案" + "\n\n");
					paper_test_id++;
				}
				dataSet = sqlServer.GetDataSet("select tb_Question_Library.question_test,tb_Question_Library.question_answer,tb_Question_Library.question_type from tb_Question_Library where tb_Question_Library.question_id=" + item);
				question_test.Append(paper_test_id.ToString() + "." + dataSet.Tables[0].Rows[0]["question_test"] + "\n");
				question_answer.Append(paper_test_id.ToString() + "." + dataSet.Tables[0].Rows[0]["question_answer"] + " ");
				question_type = dataSet.Tables[0].Rows[0]["question_type"].ToString();
				paper_test_id++;
			}
			//显示试题内容
			Add_QuestionTest_Single(richTextBox1, question_test, 11);
			Add_QuestionTest_Single(richTextBox2, question_answer, 10);
		}
		//打印试卷
		private void button1_Click(object sender, EventArgs e)
		{
			richTextBoxPrintClass richTextBoxPrint = new richTextBoxPrintClass();
			richTextBoxPrint.richTextBox = richTextBox1;
			richTextBoxPrint.ShowPrintDlg(PublicStaticClass.TestPaper.Papertitle);
		}
		/// <summary>
		/// 添加题目
		/// </summary>
		/// <param name="richTextBox">控件</param>
		/// <param name="test">题目</param>
		/// <param name="size">字体</param>
		private void Add_QuestionTest_Single(RichTextBox richTextBox, StringBuilder test, int size)
		{
			richTextBox.Multiline = true;
			richTextBox.ScrollBars = RichTextBoxScrollBars.Vertical;
			richTextBox.SelectionFont = new Font("宋体", size, FontStyle.Regular);
			richTextBox.AppendText(test.ToString());
		}

		private void Add_QuestionAnswer_Single(RichTextBox richTextBox, StringBuilder test, int size)
		{
			richTextBox.Multiline = true;
			richTextBox.ScrollBars = RichTextBoxScrollBars.Vertical;
			richTextBox.SelectionFont = new Font("宋体", size, FontStyle.Regular);
			richTextBox.AppendText(test.ToString());
		}
		//打印答案
		private void button3_Click(object sender, EventArgs e)
		{
			richTextBoxPrintClass richTextBoxPrint = new richTextBoxPrintClass();
			richTextBoxPrint.richTextBox = richTextBox2;
			richTextBoxPrint.ShowPrintDlg(PublicStaticClass.TestPaper.Papertitle+"标准答案");
		}
	}
}
