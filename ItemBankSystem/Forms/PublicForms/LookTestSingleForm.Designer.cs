﻿namespace ItemBankSystem.Forms.PublicForms
{
	partial class LookTestSingleForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.btn_return = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tbx_time = new System.Windows.Forms.TextBox();
			this.tbx_type = new System.Windows.Forms.TextBox();
			this.tbx_classes = new System.Windows.Forms.TextBox();
			this.rtbx_answer = new System.Windows.Forms.RichTextBox();
			this.rtbx_test = new System.Windows.Forms.RichTextBox();
			this.tbx_id = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(23, 37);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(65, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "试题编号：";
			// 
			// btn_return
			// 
			this.btn_return.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_return.BackColor = System.Drawing.Color.CornflowerBlue;
			this.btn_return.ForeColor = System.Drawing.Color.White;
			this.btn_return.Location = new System.Drawing.Point(655, 482);
			this.btn_return.Name = "btn_return";
			this.btn_return.Size = new System.Drawing.Size(115, 38);
			this.btn_return.TabIndex = 1;
			this.btn_return.Text = "返回";
			this.btn_return.UseVisualStyleBackColor = false;
			this.btn_return.Click += new System.EventHandler(this.btn_return_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(23, 75);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 12);
			this.label2.TabIndex = 2;
			this.label2.Text = "课程：";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(23, 109);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(65, 12);
			this.label3.TabIndex = 3;
			this.label3.Text = "试题内容：";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(23, 367);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(65, 12);
			this.label4.TabIndex = 4;
			this.label4.Text = "试题答案：";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(254, 37);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(65, 12);
			this.label5.TabIndex = 5;
			this.label5.Text = "试题类型：";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(254, 75);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(65, 12);
			this.label6.TabIndex = 6;
			this.label6.Text = "添加时间：";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.BackColor = System.Drawing.Color.Silver;
			this.groupBox1.Controls.Add(this.tbx_time);
			this.groupBox1.Controls.Add(this.tbx_type);
			this.groupBox1.Controls.Add(this.tbx_classes);
			this.groupBox1.Controls.Add(this.rtbx_answer);
			this.groupBox1.Controls.Add(this.rtbx_test);
			this.groupBox1.Controls.Add(this.tbx_id);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.btn_return);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(776, 526);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "查看试题";
			// 
			// tbx_time
			// 
			this.tbx_time.Location = new System.Drawing.Point(325, 72);
			this.tbx_time.Name = "tbx_time";
			this.tbx_time.ReadOnly = true;
			this.tbx_time.Size = new System.Drawing.Size(100, 21);
			this.tbx_time.TabIndex = 14;
			// 
			// tbx_type
			// 
			this.tbx_type.Location = new System.Drawing.Point(325, 34);
			this.tbx_type.Name = "tbx_type";
			this.tbx_type.ReadOnly = true;
			this.tbx_type.Size = new System.Drawing.Size(100, 21);
			this.tbx_type.TabIndex = 13;
			// 
			// tbx_classes
			// 
			this.tbx_classes.Location = new System.Drawing.Point(95, 72);
			this.tbx_classes.Name = "tbx_classes";
			this.tbx_classes.ReadOnly = true;
			this.tbx_classes.Size = new System.Drawing.Size(100, 21);
			this.tbx_classes.TabIndex = 12;
			// 
			// rtbx_answer
			// 
			this.rtbx_answer.Location = new System.Drawing.Point(25, 382);
			this.rtbx_answer.Name = "rtbx_answer";
			this.rtbx_answer.ReadOnly = true;
			this.rtbx_answer.Size = new System.Drawing.Size(745, 94);
			this.rtbx_answer.TabIndex = 11;
			this.rtbx_answer.Text = "";
			// 
			// rtbx_test
			// 
			this.rtbx_test.Location = new System.Drawing.Point(25, 125);
			this.rtbx_test.Name = "rtbx_test";
			this.rtbx_test.ReadOnly = true;
			this.rtbx_test.Size = new System.Drawing.Size(745, 239);
			this.rtbx_test.TabIndex = 10;
			this.rtbx_test.Text = "";
			// 
			// tbx_id
			// 
			this.tbx_id.Location = new System.Drawing.Point(95, 34);
			this.tbx_id.Name = "tbx_id";
			this.tbx_id.ReadOnly = true;
			this.tbx_id.Size = new System.Drawing.Size(100, 21);
			this.tbx_id.TabIndex = 9;
			// 
			// LookTestSingleForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Silver;
			this.ClientSize = new System.Drawing.Size(800, 550);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "LookTestSingleForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "LookTestSingleForm";
			this.Load += new System.EventHandler(this.LookTestSingleForm_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btn_return;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox tbx_id;
		private System.Windows.Forms.RichTextBox rtbx_answer;
		private System.Windows.Forms.RichTextBox rtbx_test;
		private System.Windows.Forms.TextBox tbx_time;
		private System.Windows.Forms.TextBox tbx_type;
		private System.Windows.Forms.TextBox tbx_classes;
	}
}