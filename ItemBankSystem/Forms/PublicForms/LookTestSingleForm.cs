﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;

namespace ItemBankSystem.Forms.PublicForms
{
	public partial class LookTestSingleForm : Form
	{
		public LookTestSingleForm()
		{
			InitializeComponent();

		}
		//关闭窗体
		private void btn_return_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void LookTestSingleForm_Load(object sender, EventArgs e)
		{
			tbx_classes.Text = PublicStaticClass.TestQuestion.Classes;
			tbx_id.Text = PublicStaticClass.TestQuestion.Questionid.ToString();
			tbx_time.Text = PublicStaticClass.TestQuestion.Questiontime.ToString();
			tbx_type.Text = PublicStaticClass.TestQuestion.Questiontype;
			rtbx_test.Text = PublicStaticClass.TestQuestion.Questiontest;
			rtbx_answer.Text = PublicStaticClass.TestQuestion.Questionanswer;
		}
	}
}
