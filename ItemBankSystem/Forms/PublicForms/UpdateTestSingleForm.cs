﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ItemBankSystem.Classes;

namespace ItemBankSystem.Forms.PublicForms
{
	public partial class UpdateTestSingleForm : Form
	{
		public UpdateTestSingleForm()
		{
			InitializeComponent();
		}
		//返回
		private void btn_return_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		List<string> strs = new List<string>();
		private void UpdateTestSingleForm_Load(object sender, EventArgs e)
		{
			tbx_id.Text = PublicStaticClass.TestQuestion.Questionid.ToString();
			tbx_time.Text = PublicStaticClass.TestQuestion.Questiontime.ToString();
			rtbx_test.Text = PublicStaticClass.TestQuestion.Questiontest;
			rtbx_answer.Text = PublicStaticClass.TestQuestion.Questionanswer;

			cbx_classes.Text = PublicStaticClass.TestQuestion.Classes;
			cbx_type.Text = PublicStaticClass.TestQuestion.Questiontype;
		}
		//修改试题
		private void tbn_Update_Click(object sender, EventArgs e)
		{
			string up_test = "";
			string up_answer = "";
			string up_classes = "";
			string up_type = "";
			if (rtbx_test.Text != PublicStaticClass.TestQuestion.Questiontest)
			{
				up_test = "question_test='" + rtbx_test.Text + "'";
				strs.Add(up_test);
			}
			if (rtbx_answer.Text != PublicStaticClass.TestQuestion.Questionanswer)
			{
				up_answer = "question_answer='" + rtbx_answer.Text + "'";
				strs.Add(up_answer);
			}
			if (cbx_classes.Text != PublicStaticClass.TestQuestion.Classes)
			{
				up_classes = "question_test='" + cbx_classes.Text + "'";
				strs.Add(up_classes);
			}
			if (cbx_type.Text != PublicStaticClass.TestQuestion.Questiontype)
			{
				up_type = "question_answer='" + cbx_type.Text + "'";
				strs.Add(up_type);
			}
			if (up_answer == null || up_classes == null || up_test == null || up_type == null || strs.Count == 0)
			{
				MessageBox.Show("没有要修改的内容", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			else
			{
				strs.Add("question_time=" + DateTime.Now.ToString("yyyy-MM-dd").Replace("-", ""));
				SqlServerEmploy sqlServer = new SqlServerEmploy();
				string sql_str = "update tb_Question_Library set " + PublicStaticClass.TransForms(strs, ",") + " where question_id=" + PublicStaticClass.TestQuestion.Questionid;
				strs.Clear();
				if (sqlServer.Getsqlcom(sql_str) > 0)
				{
					MessageBox.Show("修改试题成功！", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
					if (PublicStaticClass.Form_name == "LookTestForm")
					{
						PublicStaticClass.LookTest.Refresh_data();
					}
					else if (PublicStaticClass.Form_name == "FindTestQuestionForm")
					{
						PublicStaticClass.FindTestQuestion.Refresh_data();
					}
					this.Close();
				}
				else
				{
					MessageBox.Show("修改试题失败！", "软件提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}
	}
}
